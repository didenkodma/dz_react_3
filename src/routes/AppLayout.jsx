import { Outlet } from "react-router-dom";
import Header from "../components/Header";
import Footer from "../components/Footer";

function AppLayout(props) {
    const {cart, pickedOut} = props;

    return(
        <>
            <Header cart={cart} pickedOut={pickedOut} />

            <Outlet />

            <Footer />
        </>
    );
}

export default AppLayout;