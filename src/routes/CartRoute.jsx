import CartSection from "../components/CartSection";
import Modal from "../components/Modal/Modal";

function CartRoute(props) {

    const { cart, modal, changeActiveProduct, visible, activeProduct, removeFromCart } = props;

    return (
        <>

            <main className='main'>

                <CartSection cart={cart} showModal={modal}  changeActiveProduct={changeActiveProduct}/>

            </main>

            <Modal isCart={true} header="Remove this product from cart?" text="You can remove this product from cart or undo removing by closing the window." visible={visible} hideModal={modal} changeActiveProduct={changeActiveProduct} removeFromCart={removeFromCart} activeProduct={activeProduct} />
        </>
    );

}

export default CartRoute;