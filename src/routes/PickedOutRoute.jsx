import PickedOutSection from "../components/PickedOutSection/PickedOutSection";
import Modal from "../components/Modal/Modal";

function PickedOutRoute(props) {

    const { pickedOut, modal, addToPickedOut, removeFromPickedOut, changeActiveProduct, visible, addToCart, activeProduct } = props;

    return (
        <>

            <main className='main'>

                <PickedOutSection pickedOut={pickedOut} showModal={modal} addToPickedOut={addToPickedOut} removeFromPickedOut={removeFromPickedOut} changeActiveProduct={changeActiveProduct}/>

            </main>

            <Modal header="Add this product to cart?" text="You can add this product to cart or undo adding by closing the window." visible={visible} hideModal={modal} changeActiveProduct={changeActiveProduct} addToCart={addToCart} activeProduct={activeProduct} />
        </>
    );

}

export default PickedOutRoute;