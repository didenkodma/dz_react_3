import CarouselSection from "../components/CarouselSection";
import WelcomeSection from "../components/WelcomeSection";
import ProductsSection from "../components/ProductsSection";
import PartnersSection from "../components/PartnersSection";
import Modal from "../components/Modal/Modal";

function Home(props) {

    const { goods, modal, addToPickedOut, removeFromPickedOut, changeActiveProduct, visible,  addToCart, activeProduct } = props;

    return (
        <>

            <main className='main'>

                <CarouselSection />

                <WelcomeSection />

                <ProductsSection goods={goods} showModal={modal} addToPickedOut={addToPickedOut} removeFromPickedOut={removeFromPickedOut} changeActiveProduct={changeActiveProduct} />

                <PartnersSection />

            </main>

            <Modal header="Add this product to cart?" text="You can add this product to cart or undo adding by closing the window." visible={visible} hideModal={modal} changeActiveProduct={changeActiveProduct} addToCart={addToCart} activeProduct={activeProduct} />
        </>
    );

}

export default Home;