import "./ProductsList.scss";
import Product from '../Product';
import PropTypes from 'prop-types';

function ProductsList(props) {

    const { goods, showModal, addToPickedOut, removeFromPickedOut, changeActiveProduct } = props;
    
    return (
        <div className='products__list'>
            {goods.map(element => {
                const { color, imgSrc, name, price, productCode, text } = element;
                return <Product key={productCode} color={color} imgSrc={imgSrc} name={name} price={price} productCode={productCode} text={text} showModal={showModal} addToPickedOut={addToPickedOut} removeFromPickedOut={removeFromPickedOut} changeActiveProduct={changeActiveProduct} />
            })}
        </div>
    );
}


ProductsList.propTypes = {
    goods: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    addToPickedOut: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired,
    removeFromPickedOut: PropTypes.oneOfType([PropTypes.func, PropTypes.bool])
}

ProductsList.defaultProps = {
    removeFromPickedOut: false
}

export default ProductsList;