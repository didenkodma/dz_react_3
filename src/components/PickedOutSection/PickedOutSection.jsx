import "./PickedOutSection.scss";
import PickedOutList from "../PickedOutList/PickedOutList";
import PropTypes from 'prop-types';

function PickedOutSection(props) {

    const {pickedOut, showModal, addToPickedOut, removeFromPickedOut, changeActiveProduct} = props;

    return (
        <section className='picked-out-section'>

            <div className='container'>

                <div className='picked-out-section__content'>
                    <h2>Picked Out</h2>
                    {
                        pickedOut.length > 0
                            ?
                            <PickedOutList pickedOut={pickedOut} showModal={showModal} addToPickedOut={addToPickedOut} removeFromPickedOut={removeFromPickedOut} changeActiveProduct={changeActiveProduct} />
                            :
                            <p className='picked-out-section__no-items'>No items has been added.</p>
                    }
                </div>
            </div>

        </section>
    );
}


PickedOutSection.propTypes = {
    pickedOut: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired,
    removeFromPickedOut: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
    addToPickedOut: PropTypes.oneOfType([PropTypes.func, PropTypes.bool])
}

PickedOutSection.defaultProps = {
    removeFromPickedOut: false,
    addToPickedOut: false
}

export default PickedOutSection;