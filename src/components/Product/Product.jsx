import "./Product.scss";
import Button from '../Button/Button';
import Icon from '../Icon';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

function Product(props) {

    const { isCart, color, imgSrc, name, price, productCode, text, addToPickedOut, removeFromPickedOut, showModal, changeActiveProduct } = props;

    const activeProduct = { color, imgSrc, name, price, productCode, text };

    const priceCustomStyle = {
        color: color,
    }

    const [isPickedOut, setIsPickedOut] = useState(false);


    const clickPickedOutHandler = () => {
        isPickedOut ? removeFromPickedOut(productCode) : addToPickedOut(activeProduct);
        setIsPickedOut(!isPickedOut);
    }

    const clickAddToCartHandler = () => {
        showModal();
        changeActiveProduct(activeProduct);
    }

    useEffect(() => {

        const pickedOutProducts = JSON.parse(localStorage.getItem('pickedOut'));

        if (pickedOutProducts.find(e => e.productCode === productCode)) {
            setIsPickedOut(true);
        }
    }, [productCode])


    return (
        <div className='product'>
            <img className='product__img' src={imgSrc} alt={`Product ${productCode}`} />
            <div className='product__content'>
                <h4 className='product__heading'>{name}</h4>
                <p className='product__text'>{text}</p>
                <div className='product__add'>
                    <span className='product__price' style={priceCustomStyle}>{price}</span>
                    { !isCart
                    ?
                    <Button backgroundColor="#1e1e20" padding=".9rem .5rem" text='Add to cart' onClick={clickAddToCartHandler} />
                    :
                    <Button backgroundColor="#1e1e20" padding=".9rem 2.8rem" text='Buy' onClick={() => console.log(`The good ${productCode} has been bought`)} />
                    }
                </div>
                {
                    !isCart &&
                    <button className='product__picked-out' onClick={clickPickedOutHandler}>
                        {isPickedOut
                            ?
                            <Icon color="#cc3333" type="star-fill" />
                            :
                            <Icon color="#cc3333" type="star" />
                        }
                    </button>
                }

            </div>
        </div>
    );
}


Product.propTypes = {
    isCart: PropTypes.bool,
    color: PropTypes.string,
    imgSrc: PropTypes.string.isRequired,
    name: PropTypes.string,
    price: PropTypes.string,
    productCode: PropTypes.string.isRequired,
    text: PropTypes.string,
    showModal: PropTypes.func.isRequired,
    addToPickedOut: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    changeActiveProduct: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    removeFromPickedOut: PropTypes.oneOfType([PropTypes.bool, PropTypes.func])
}

Product.defaultProps = {
    isCart: false,
    color: '#1e1e20',
    name: 'Some product on sale',
    price: '$0.00',
    text: 'Some dummy product text',
    addToPickedOut: false,
    changeActiveProduct: false,
    removeFromPickedOut: false
}

export default Product;