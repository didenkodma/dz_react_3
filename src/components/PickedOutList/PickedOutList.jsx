import "./PickedOutList.scss";
import Product from '../Product';
import PropTypes from 'prop-types';

function PickedOutList(props) {

    const { pickedOut, showModal, addToPickedOut, removeFromPickedOut, changeActiveProduct } = props;
    
    return (
        <div className='picked-out-section__list'>
            {pickedOut.map(element => {
                const { color, imgSrc, name, price, productCode, text } = element;
                return <Product key={productCode} color={color} imgSrc={imgSrc} name={name} price={price} productCode={productCode} text={text} showModal={showModal} addToPickedOut={addToPickedOut} removeFromPickedOut={removeFromPickedOut} changeActiveProduct={changeActiveProduct} />
            })}
        </div>
    );
}


PickedOutList.propTypes = {
    pickedOut: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    addToPickedOut: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired,
    removeFromPickedOut: PropTypes.oneOfType([PropTypes.func, PropTypes.bool])
}

PickedOutList.defaultProps = {
    removeFromPickedOut: false
}

export default PickedOutList;