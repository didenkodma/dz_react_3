import "./PickedOut.scss";
import { Link } from "react-router-dom";
import Icon from '../Icon';
import PropTypes from 'prop-types';

function PickedOut(props) {
    return (
        <Link to='/picked-out' className='picked-out'>

            <Icon color="#fff" type="star-fill" />
            <span>({props.pickedOut.length})</span>
        </Link>
    );
}


PickedOut.propTypes = {
    pickedOut: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default PickedOut;