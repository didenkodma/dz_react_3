import "./Cart.scss";
import { Link } from "react-router-dom";
import Icon from '../Icon';
import PropTypes from 'prop-types';

function Cart(props) {

    return (
        <Link to="/cart" className='cart'>

            <Icon color="#fff" type="cart" />
            <span>({props.cart.length})</span>
        </Link>
    );
}


Cart.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Cart;