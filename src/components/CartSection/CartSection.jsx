import "./CartSection.scss";
import CartList from '../CartList';
import PropTypes from 'prop-types';

function CartSection(props) {

    const {cart, showModal, changeActiveProduct} = props;

    return (
        <section className='cart-section'>

            <div className='container'>

                <div className='cart-section__content'>
                    <h2>Cart</h2>
                    {
                        cart.length > 0
                            ?
                            <CartList cart={cart} showModal={showModal} changeActiveProduct={changeActiveProduct} />
                            :
                            <p className='cart-section__no-items'>No items has been added.</p>
                    }
                </div>
            </div>

        </section>
    );
}


CartSection.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired
}

export default CartSection;