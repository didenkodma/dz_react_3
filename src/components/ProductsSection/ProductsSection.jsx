import "./ProductsSection.scss";
import ProductsList from '../ProductsList';
import PropTypes from 'prop-types';

function ProductsSection(props) {

        return (
            <section className='products'>

                <div className='container'>

                    <div className='products__content'>
                        <h2>Albums Currently on sale</h2>

                        <ProductsList goods={props.goods} showModal={props.showModal} addToPickedOut={props.addToPickedOut} removeFromPickedOut={props.removeFromPickedOut} changeActiveProduct={props.changeActiveProduct} />
                    </div>
                </div>

            </section>
        );
    }


ProductsSection.propTypes = {
    goods: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    addToPickedOut: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired,
    removeFromPickedOut: PropTypes.oneOfType([PropTypes.func, PropTypes.bool])
}

ProductsSection.defaultProps = {
    removeFromPickedOut: false
}

export default ProductsSection;