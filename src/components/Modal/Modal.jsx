import './Modal.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

function Modal(props) {

    const { isCart, header, text, removeFromCart, closeButton, visible, hideModal, changeActiveProduct, activeProduct, addToCart } = props;

    const clickCloseHandler = () => {
        hideModal();
        changeActiveProduct({});
    }

    const clickAddHandler = () => {
        addToCart(activeProduct);
        hideModal();
        changeActiveProduct({});
    }

    const removeFromCartHandler = () => {
        removeFromCart(activeProduct.id);
        hideModal();
        changeActiveProduct({});
    }

    return visible ? (
        <div className='modal' onClick={clickCloseHandler}>
            <div className='modal-content' onClick={(e) => {
                e.stopPropagation();
            }}>
                <div className='modal-top'>
                    <h2 className='modal-header'>{header}</h2>
                </div>
                <div className='modal-bottom'>
                    <p className='modal-text'>{text}</p>
                    <div className='modal-button-container'>

                        {
                            !isCart
                                ?
                                <Button backgroundColor="#1e1e20" padding="1.3rem 3.3rem" text='Add' onClick={clickAddHandler} />
                                :
                                <Button backgroundColor="#1e1e20" padding="1.3rem 2.5rem" text='Remove' onClick={removeFromCartHandler} />
                        }

                        <Button backgroundColor="#1e1e20" padding="1.3rem 2.7rem" text='Close' onClick={clickCloseHandler} />

                    </div>
                </div>
                {closeButton && <button className='modal-close-btn' onClick={clickCloseHandler}></button>}
            </div>
        </div>
    ) : null;
}


Modal.propTypes = {
    isCart: PropTypes.bool,
    header: PropTypes.string,
    text: PropTypes.string,
    closeButton: PropTypes.bool,
    visible: PropTypes.bool.isRequired,
    hideModal: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired,
    addToCart: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
    removeFromCart: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
    activeProduct: PropTypes.object.isRequired
}

Modal.defaultProps = {
    isCart: false,
    header: "Modal",
    text: 'Some modal dummy text',
    closeButton: false,
    addToCart: false,
    removeFromCart: false
}

export default Modal;