import "./Header.scss";
import HeaderTop from '../HeaderTop';
import HeaderBottom from '../HeaderBottom';
import PropTypes from 'prop-types';

function Header(props) {

    const {cart, pickedOut} = props;

    return (
        <header className='header' id="header">

            <HeaderTop cart={cart} pickedOut={pickedOut} />

            <HeaderBottom />

        </header>
    );
}


Header.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    pickedOut: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Header;