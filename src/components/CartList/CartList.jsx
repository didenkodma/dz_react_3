import "./CartList.scss";
import Product from '../Product';
import PropTypes from 'prop-types';

function CartList(props) {

    const { cart, showModal, changeActiveProduct } = props;

    return (
        <div className='cart-section__list'>
            {cart.map((element, id) => {
                const { color, imgSrc, name, price, productCode, text } = element;
                const activeProduct = { id, color, imgSrc, name, price, productCode, text };
                const clickDeleteHandler = () => {
                    changeActiveProduct(activeProduct);
                    showModal();
                }
                return <div key={id} className="cart-section__list-wrapper">
                    <Product isCart={true} color={color} imgSrc={imgSrc} name={name} price={price} productCode={productCode} text={text} showModal={showModal} />
                    <button className='cart-section__close-btn' onClick={clickDeleteHandler}></button>
                </div>
            })}
        </div>
    );
}


CartList.propTypes = {
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    showModal: PropTypes.func.isRequired,
    changeActiveProduct: PropTypes.func.isRequired
}

export default CartList;