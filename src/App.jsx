import { useState, useEffect, useRef } from 'react';
import { createBrowserRouter, Navigate, RouterProvider} from "react-router-dom";
import AppLayout from './routes/AppLayout';
import Home from './routes/Home';
import CartRoute from './routes/CartRoute';
import PickedOutRoute from './routes/PickedOutRoute';
import ErrorPage from './routes/ErrorPage';

function App() {

    const [goods, setGoods] = useState([]);

    const [isModalVisible, setIsModalVisible] = useState(false);

    const [cart, setCart] = useState([]);

    const [pickedOut, setPickedOut] = useState([]);

    const [active, setActive] = useState({});

    const didMountCart = useRef(false);
    const didMountPickedOut = useRef(false);

    const modal = () => {
        setIsModalVisible(!isModalVisible);
    }

    const changeActiveProduct = (productObj) => {
        setActive(productObj);
    }

    const addToCart = (productObj) => {
        setCart(() => [...cart, productObj])
    }

    const removeFromCart = (index) => {
        const arr = cart;
        arr.splice(index, 1);
        setCart(() => [...arr])
    }

    const addToPickedOut = (productObj) => {
        if (!pickedOut.find(e => e.productCode === productObj.productCode)) {
            setPickedOut(() => [...pickedOut, productObj])
        }
    }

    const removeFromPickedOut = (productCode) => {
        const index = pickedOut.map(e => e.productCode).indexOf(productCode);
        const arr = pickedOut;
        arr.splice(index, 1);
        setPickedOut(() => [...arr])
    }

    const router = createBrowserRouter([
        {
            path: "/",
            element: <AppLayout cart={cart} pickedOut={pickedOut} />,
            errorElement: <ErrorPage />,
            children: [
                {
                    index: true,
                    element: <Navigate to='shop' replace />
                },
                {
                    path: 'shop',
                    element: <Home goods={goods} modal={modal} addToPickedOut={addToPickedOut} removeFromPickedOut={removeFromPickedOut} changeActiveProduct={changeActiveProduct} visible={isModalVisible} addToCart={addToCart} activeProduct={active} />
                },
                {
                    path: 'cart',
                    element: <CartRoute cart={cart} removeFromCart={removeFromCart} modal={modal} changeActiveProduct={changeActiveProduct} visible={isModalVisible} activeProduct={active} />
                },
                {
                    path: 'picked-out',
                    element: <PickedOutRoute pickedOut={pickedOut} modal={modal} addToPickedOut={addToPickedOut} removeFromPickedOut={removeFromPickedOut} changeActiveProduct={changeActiveProduct} visible={isModalVisible} addToCart={addToCart} activeProduct={active} />
                }
            ]
        }
    ]);

    useEffect(() => {

        const cart = JSON.parse(localStorage.getItem('cart'));
        const pickedOut = JSON.parse(localStorage.getItem('pickedOut'));

        cart ? setCart(cart) : localStorage.setItem('cart', '[]');
        pickedOut ? setPickedOut(pickedOut) : localStorage.setItem('pickedOut', '[]');

    }, [])

    useEffect(() => {

        fetch('products.json')
            .then(res => res.json())
            .then(response => setGoods(response))

    }, [])

    useEffect(() => {

        if (!didMountCart.current) {
            didMountCart.current = true;
        } else {
            localStorage.setItem('cart', JSON.stringify(cart));
        }


    }, [cart])


    useEffect(() => {

        if (!didMountPickedOut.current) {
            didMountPickedOut.current = true;
        } else {
            localStorage.setItem('pickedOut', JSON.stringify(pickedOut));
        }

    }, [pickedOut])

    return <RouterProvider router={router} />;
}

export default App;
